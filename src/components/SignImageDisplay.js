import React from 'react';
import '../styles/signImageDisplay.css';

const SignImageDisplay = (props) => {
  //If the character is a space, render a blank div
  if (props.image === ' ') {
    return <div id='space'></div>;
  }

  //Render the image corresponding to each character
  return (
    <div>
      <img
        id='sign'
        src={require('../utils/images/' + props.image + '.png')}
        alt={props.image}
      ></img>
    </div>
  );
};

export default SignImageDisplay;
