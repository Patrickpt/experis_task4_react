import React from 'react';
import '../styles/header.css';
import { useHistory } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';
import { getStorage } from '../utils/storage';
import { setUserAction } from '../store/actions/user';

function Header(props) {
  let user = useSelector((state) => state.user);
  const history = useHistory();
  const dispatcher = useDispatch();

  const navigateProfile = () => {
    history.push('/profile');
  };

  if (getStorage('name')) {
    dispatcher(setUserAction(getStorage('name')));
  }

  return (
    <div>
      <div id='headerContainer'>
        <div id='mainTitle'>
          <h1 id='heading'>Lost In Translation</h1>
        </div>
        <div id='userTitle'>
          <h3 id='user' onClick={navigateProfile}>
            {user}
          </h3>
        </div>
      </div>
      <div id='headerUnderline'></div>
    </div>
  );
}

export default Header;
