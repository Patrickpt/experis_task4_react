import React from 'react';
import './App.css';
import LoginView from './views/LoginView';
import Translate from './views/Translate';
import { BrowserRouter as Router, Route, Redirect } from 'react-router-dom';
import ProfileView from './views/ProfileView';
import Header from '../src/components/Header';

function App() {
  return (
    <Router>
      <div className='App'>
        <Header></Header>
        <Route exact path='/'>
          <Redirect to='/login'></Redirect>
        </Route>
        <Route exact path='/login' component={LoginView}></Route>
        <Route path='/translate' component={Translate}></Route>
        <Route path='/profile' component={ProfileView}></Route>
        <Route path='/'>
          <Redirect to='/login'></Redirect>
        </Route>
      </div>
    </Router>
  );
}

export default App;
