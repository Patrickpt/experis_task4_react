//Types
export const ACTION_SET_USER = 'ACTION_SET_USER';

//Actions
export const setUserAction = (user = '') => ({
  type: ACTION_SET_USER,
  payload: user,
});
