import { createStore, combineReducers } from 'redux';
import userReducer from './reducers/user';

const rootReducers = combineReducers({
  user: userReducer,
});

export default createStore(
  rootReducers,
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
);
