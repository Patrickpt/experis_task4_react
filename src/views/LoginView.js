import React, { useState, useEffect } from 'react';
import '../styles/loginView.css';
import { setStorage } from '../utils/storage';
import { FilledInput, Button } from '@material-ui/core/';
import { getStorage } from '../utils/storage';
import { Redirect } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import { setUserAction } from '../store/actions/user';

function LoginView() {
  const [name, setName] = useState('');
  const [isLoggedIn, setIsLoggedIn] = useState(false);
  const dispatcher = useDispatch();

  useEffect(() => {
    setIsLoggedIn(getStorage('name'));
  }, []);

  //Submit the name if the user clicks enter or the start button (keyCode undefined)
  const handleSubmit = (e) => {
    if (e.keyCode === 13 || e.keyCode === undefined) {
      if (name !== '') {
        setStorage('name', name);
        setIsLoggedIn(true);
        dispatcher(setUserAction(name));
      } else {
        alert('Please enter your name');
      }
    }
  };

  return (
    <div>
      {isLoggedIn && <Redirect to='/translate'></Redirect>}
      <div id='container'>
        <div id='topContainer'>
          <div id='logo'>
            <img
              id='logoImg'
              src={require('../utils/images/Logo-Hello.png')}
              alt='logo'
              height='180px'
            ></img>
          </div>
          <div id='titleContainer'>
            <div>
              <h1 className='pageText'>Lost In Translation</h1>
            </div>
            <div>
              <h3 className='pageText'>Get started</h3>
            </div>
          </div>
        </div>
        <div id='inputContainer'>
          <FilledInput
            id='inputName'
            onKeyDown={handleSubmit}
            placeholder="What's your name?"
            onChange={(event) => setName(event.target.value)}
          />
          <Button
            id='submitButton'
            variant='contained'
            color='primary'
            onClick={handleSubmit}
          >
            Start!
          </Button>
        </div>
      </div>
    </div>
  );
}

export default LoginView;
