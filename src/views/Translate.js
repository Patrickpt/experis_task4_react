import React, { useEffect, useState } from 'react';
import '../styles/translate.css';
import { FilledInput, Icon, Button } from '@material-ui/core/';
import { getStorage, setStorage } from '../utils/storage';
import SignImageDisplay from '../components/SignImageDisplay';
import { Redirect } from 'react-router-dom';

function Translate() {
  const [translate, setTranslate] = useState('');
  const [imageNames, setImageNames] = useState([]);
  const [isLoggedIn, setIsLoggedIn] = useState(true);

  useEffect(() => {
    setIsLoggedIn(getStorage('name'));
  }, []);

  //Pass in a the new translation to add to localstorage,
  // remove the oldest translation if there are more than 10 stored
  const storeTranslations = (newTranslation) => {
    let storedTranslations = '';
    if (getStorage('prevTranslations')) {
      storedTranslations = getStorage('prevTranslations');
    }
    storedTranslations = storedTranslations.toString();
    storedTranslations = storedTranslations
      ? storedTranslations.split(',')
      : [];
    if (storedTranslations.length >= 10) {
      storedTranslations.pop();
    }
    storedTranslations.unshift(newTranslation);
    setStorage('prevTranslations', storedTranslations.toString());
  };

  //Only accept a-z, spaces, and phrases less that 25 chars
  const validateInput = (chars) => {
    let charString = chars.join('');
    if (!/^[a-zA-Z\s]+$/.test(charString)) {
      alert('Your word(s) can only contain alphabetical characters and spaces');
      return false;
    } else if (chars.length > 24) {
      alert('Your word/phrase can not contain more than 24 characters');
      return false;
    }
    return true;
  };

  const handleTranslate = () => {
    const chars = translate.toLowerCase().split('');
    if (validateInput(chars)) {
      storeTranslations(translate);
      setImageNames(chars);
    }
  };

  const signList = imageNames.map((sign) => {
    return (
      <SignImageDisplay image={sign} key={Math.random()}></SignImageDisplay>
    );
  });

  return (
    <div>
      {!isLoggedIn && <Redirect to='/login'></Redirect>}
      <div id='translateContainer'>
        <div id='inputTranslateContainer'>
          <FilledInput
            id='inputTranslate'
            placeholder='Something you want to translate'
            onChange={(event) => setTranslate(event.target.value)}
          />
          <Button
            id='translateButton'
            variant='contained'
            color='primary'
            endIcon={<Icon>send</Icon>}
            onClick={handleTranslate}
          >
            Translate
          </Button>
        </div>
        <div id='displayCard'>{signList}</div>
      </div>
    </div>
  );
}

export default Translate;
