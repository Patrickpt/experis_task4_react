import React, { useState, useEffect } from 'react';
import { getStorage } from '../utils/storage';
import { Button } from '@material-ui/core';
import { Redirect } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import { setUserAction } from '../store/actions/user';

function ProfileView() {
  const [user, setUser] = useState('');
  const [prevTranslations, setPrevTranslations] = useState([]);
  const [isLoggedIn, setIsLoggedIn] = useState(true);
  const dispatcher = useDispatch();

  useEffect(() => {
    setUser(getStorage('name'));
    setIsLoggedIn(getStorage('name'));
    if (getStorage('prevTranslations')) {
      setPrevTranslations(getStorage('prevTranslations').split(','));
    }
  }, []);

  const prevTranslationsList = prevTranslations.map((translation) => {
    return <li key={translation}> {translation} </li>;
  });

  const logOutAndDeleteData = () => {
    localStorage.clear();
    setIsLoggedIn(false);
    dispatcher(setUserAction(''));
  };

  return (
    <div>
      {!isLoggedIn && <Redirect to='/login'></Redirect>}
      <h2>
        {user}'s {prevTranslations.length} last translations
      </h2>
      <ul>{prevTranslationsList}</ul>
      <Button
        variant='contained'
        color='secondary'
        onClick={logOutAndDeleteData}
      >
        Delete data and log out
      </Button>
    </div>
  );
}

export default ProfileView;
